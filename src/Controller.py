# -*- coding: utf-8 -*-

#####################
#				    #
#	25/03/2020		#
#	Victor Leveneur #
#					#
#####################

import model
import json

class Controller:
    def __init__(self):
        self.workspace = None

    def _loadJSON(self,path,hook):
        try:
            with open(path,"r") as f:
                return json.load(f,object_hook=hook)
        except:
            return None
    def _saveJSON(self,o,path,cls):
        try:
            with open(path,"w") as f:
                json.dump(o,f,cls=cls)
                return True
        except:
            return False
    # def _getProject(projectId):
    #     return None if self.workspace is None else self.workspace.projects[projectId]
    # def _getComposition(projectId,compositionId):
    #     p = self._getProject(projectId)
    #     return None if p is None else p.compositions[compositionId]
    # def _getEntry(projectId,compositionId,entryId):
    #     c = self._getComposition(projectId,compositionId)
    #     return None if c is None else c.entries[entryId]

    def createNewWorkspace(self,name=None):
        self.workspace = model.Workspace(name)
        return self.workspace
    def loadWorkspace(self,path):
        w = self._loadJSON(path,model.WorkspaceJSONDecoder)
        self.workspace = w if w is not None else self.workspace
        return True if w is not None else False
    def saveWorkspace(self,path):
        if self.workspace is None:
            return True
        return self._saveJSON(self.workspace,path,model.WorkspaceJSONEncoder)

    #
    # def createProject(self,name,tags,imagePath):
    #     if self.workspace is None:
    #         return False
    #     p = model.Project(name,tags,imagePath)
    #     self.workspace.addProject(p)
    #     return True
    # def removeProject(self,projectId):
    #     if self.workspace is None:
    #         return False
    #     self.workspace.rmProject(projectId)
    #     return True
    # def loadProject(self,path):
    #     if self.workspace is None:
    #         return False
    #     p = self._loadJSON(path,model.ProgressIndicatorJSONDecoder)
    #     if p is None:
    #         return False
    #     self.workspace.addProject(p)
    #     return True
    # def saveProject(self,projectId,path):
    #     project = self._getProject(projectId)
    #     if project is None:
    #         return False
    #     return self._saveJSON(project,path,model.ProjectJSONEncoder)
    #
    # def createComposition(self,projectId,name,tags,filepath,priority,position):
    #     p = self._getProject(projectId)
    #     if p is None:
    #         return False
    #     c = model.Composition(p.progress,name,tags,filepath,priority,position)
    #     p.addComposition(c)
    #     return True
    # def removeComposition(self,projectId,compositionId):
    #     p = self._getProject(projectId)
    #     if p is None:
    #         return True
    #     p.rmComposition(compositionId)
    #     return True
    # def loadComposition(self,path,projectId):
    #     p = self._getProject(projectId)
    #     if p is None:
    #         return False
    #     c = self._loadJSON(path,model.CompositionJSONDecoder)
    #     if c is None:
    #         return False
    #     p.addComposition(c)
    #     return True
    # def saveComposition(self,path,projectId,compositionId):
    #     c = self._getComposition(projectId,compositionId)
    #     if c is None:
    #         return False
    #     return self._saveJSON(c,path,model.CompositionJSONEncoder)
    #
    # def createEntry(self,projectId,compositionId,name,indicators,tags,start,length):
    #     c = self._getComposition(projectId,compositionId)
    #     if c is None:
    #         return False
    #     e = model.Entry(c.progress,name,indicators,tags,start,length)
    #     c.addEntry(e)
    #     return True
    # def removeEntry(self,projectId,compositionId,entryId):
    #     c = self._getComposition(projectId,compositionId)
    #     if c is None:
    #         return False
    #     c.rmEntry(entryId)
    #     return True
    #
    # def createIndicator(self,projectId,compositionId,entryId,name,comment):
    #     e = self._getEntry(projectId,compositionId,entryId)
    #     if e is None:
    #         return False
    #     e.addIndicator(name,comment)
    #     return True
    # def removeIndicator(self,projectId,compositionId,entryId):
    #     e = self._getEntry(projectId,compositionId,entryId)
    #     if e is None:
    #         return False
    #     e.rmIndicator(entryId)
    #     return True
