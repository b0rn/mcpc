# -*- coding: utf-8 -*-

#####################
#				    #
#	29/02/2020		#
#	Victor Leveneur #
#					#
#####################

import uuid

class ProgressIndicator:
    def __init__(self,parent,name,comment=""):
        self.uuid = uuid.uuid1()
        self.nbChildren = 0
        self.parent = None
        if isinstance(parent,ProgressIndicator):
            self.parent = parent
            self.parent.nbChildren += 1
        self.name = name if isinstance(name,str) else ""
        self.comment = comment if isinstance(comment,str) else ""
        self.progress = 0

    def setProgress(self,progress):
        oldProgress = self.progress
        self.progress = max(min(100, progress), 0) if isinstance(progress,int) else self.progress
        if self.parent != None:
            self.parent.progress = ((self.parent.progress * self.parent.nbChildren) - oldProgress + self.progress) / self.parent.nbChildren


class AbstractEntry:
    def __init__(self,name,start,length,tags,parentProgress):
        self.uuid = uuid.uuid1()
        self.name = name if isinstance(name,str) else ""
        self.start = start if isinstance(start,int) else 0
        self.length = length if isinstance(length,int) else None
        self.progress = ProgressIndicator(parentProgress,self.name)
        self.rating = -1 # between 0 && 100
        self.tags = []
        self.comments = ""
        self.setTags(tags)

    def setTags(self,tags):
        self.tags = []
        for tag in tags:
            if isinstance(tag,str):
                self.tags.append(tag)
