# -*- coding: utf-8 -*-

#####################
#				    #
#	29/02/2020		#
#	Victor Leveneur #
#					#
#####################

from AbstractEntry import *
from Entry import *
import time,vlc,uuid

class Composition(AbstractEntry):
    def __init__(self,parentProgress,name,tags,filepath,priority,position):
        super().__init__(name,0,None,tags,parentProgress)
        self.position = position if isinstance(position,int) else None
        self.filepath = filepath if isinstance(filepath,str) else ""
        self.priority = max(min(100, priority), 0) if isinstance(priority,int) else 0
        self.entries = {}
        self.temporalComments = {}
        self.length = None
        if filepath:
            try:
                inst = vlc.Instance().media_new(self.filepath)
                inst.parse() # à changer (déprécié)
                self.length = inst.get_duration()
            except Exception as e:
                print(str(e))

    def addEntry(self,entry):
        if isinstance(entry,Entry):
            self.entries[entry.uuid] = entry
            return entry
    def rmEntry(self,uuid):
        self.entries.pop(uuid,None)

    def addTemporalComment(self,txt):
        if isinstance(txt,str):
            uuid = uuid.uuid1()
            tc = {"txt":txt,"uuid":uuid}
            self.temporalComments[uuid] = tc
            return tc
    def rmTemporalComment(self,uuid):
        self.temporalComments.pop(uuid,None)
