# -*- coding: utf-8 -*-

#####################
#				    #
#	23/03/2020		#
#	Victor Leveneur #
#					#
#####################

import json
import uuid
import numbers
from collections import namedtuple
from AbstractEntry import *
from Composition import *
from Entry import *
from Project import *
from Workspace import *

class MalformedFile(Exception):
    pass

def isNumberInRange(n,min,max):
    if isinstance(n, numbers.Number) :
        if min != None and max != None and n <= max and n >= min:
            return True
        elif min == None and n <= max:
            return True
        elif max == None and n >= min:
            return True
        elif max == None and min == None:
            return True
    return False

def checkDictForNumbersInRange(d,args):
    for key,value in args.items():
        mandatory = value[2] if len(value) >= 3 else True
        if key not in d and mandatory:
            return False
        if not isNumberInRange(d[key],value[0],value[1]):
            return False
    return True

def checkDictForTypes(d,args):
    for key,value in args.items():
        type = value[0] if isinstance(value,tuple) else value
        mandatory = value[1] if isinstance(value,tuple) and len(value) >= 2 else True
        if key not in d and mandatory:
            return False
        elif key not in d and not mandatory:
            continue
        if type == "Nb":
            if not isinstance(d[key],numbers.Number):
                return False
        elif isinstance(type,list):
            r = False
            for e in type:
                if e is None and d[key] is None:
                    r = True
                    break
                elif isinstance(d[key],e):
                    r = True
                    break
            if r == False:
                return False
        elif not isinstance(d[key],type):
            return False
    return True

def checkListType(l,type):
    if not isinstance(l,list):
        return False
    for e in l:
        if not isinstance(e,type):
            return False
    return True

def checkDictType(d,type1,type2):
    if not isinstance(d,dict):
        return False
    for key,value in d.items():
        if not isinstance(key,type1) or not isinstance(value,type2):
            return False
    return True

def deserializeP1(d):
    d["uuid"] = uuid.UUID(d["uuid"])
    d.pop("_type",None)
    return d

def dictToObject(d,type):
    ret = None
    if type == "ProgressIndicator":
        ret = ProgressIndicator("","")
    elif type == "AbstractEntry":
        ret = AbstractEntry("",0,0,[],None)
    elif type == "Entry":
        ret = Entry(None,"",[],[],0,0)
    elif type == "Composition":
        ret = Composition(None,"",[],"",0,0)
    elif type == "Project":
        ret = Project("",[],"")
    elif type == "Workspace":
        ret = Workspace("")

    if ret is not None:
        for k,v in d.items():
            if k in ret.__dict__:
                setattr(ret,k,v)
    return ret

def loadDictOfObj(d,decoder):
    tmp = {}
    for e in d:
        e = json.loads(e,object_hook=decoder)
        tmp[e.uuid] = e
    return tmp


def ProgressIndicatorJSONDecoder(dct):
    if "_type" in dct and dct["_type"] == "ProgressIndicator":
        d = deserializeP1(dct)

        numbersInRangeArg = {"nbChildren":(0,None),"progress":(0,100)}
        types = {"uuid":(uuid.UUID),"nbChildren":("Nb"),"name":(str), \
        "progress":("Nb"),"parent":([ProgressIndicator,None],False)}

        if checkDictForNumbersInRange(d,numbersInRangeArg) and checkDictForTypes(d,types):
            return dictToObject(d,"ProgressIndicator")
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")

def AbstractEntryJSONDecoder(dct,toDict=False):
    if "_type" in dct and dct["_type"] == "AbstractEntry":
        d = deserializeP1(dct)

        numbersInRangeArg = {"start":(0,None),"length":(0,None),"rating":(-1,100)}
        types = {"uuid":(uuid.UUID),"name":(str),"start":("Nb"),"length":("Nb"),"progress":(ProgressIndicator),\
        "rating":("Nb"),"tags":(list),"comments":(str)}

        d["progress"] = json.loads(d["progress"],object_hook=ProgressIndicatorJSONDecoder)

        if checkDictForNumbersInRange(d,numbersInRangeArg) and checkDictForTypes(d,types) \
        and checkListType(d["tags"],str):
            return dictToObject(d,"AbstractEntry") if not toDict else d
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")

def EntryJSONDecoder(dct):
    if "_type" in dct and dct["_type"] == "Entry":
        dct["_type"] = "AbstractEntry"
        d = AbstractEntryJSONDecoder(dct,True)

        if checkDictType(d["progressIndicators"],uuid.UUID,ProgressIndicator):
            return dictToObject(d,"Entry")
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")

def CompositionJSONDecoder(dct):
    if "_type" in dct and dct["_type"] == "Composition":
        dct["_type"] = "AbstractEntry"
        d = AbstractEntryJSONDecoder(dct,True)

        numbersInRangeArg = {"priority":(0,100)}
        types = {"position":([int,None]),"filepath":(str),"priority":(int),\
        "entries":(dict),"temporalComments":(dict),"length":([int,None],False)}

        if checkDictForNumbersInRange(d,numbersInRangeArg) and checkDictForTypes(d,types) and \
        checkDictType(d["entries"],uuid.UUID,Entry):
            for tc in d["temporalComments"]:
                if not isinstance(tc["txt"],str) or not isinstance(tc["uuid",uuid.UUID]):
                    return dct

            d["entries"] = loadDictOfObj(d["entries"],EntryJSONDecoder)
            return dictToObject(d,"Composition")
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")

def ProjectJSONDecoder(dct):
    if "_type" in dct and dct["_type"] == "Project":
        dct["_type"] = "AbstractEntry"
        d = AbstractEntryJSONDecoder(dct,True)

        types = {"imagePath":([str,None])}

        if checkDictType(d["compositions"],uuid.UUID,Composition) and checkDictForTypes(d,types):
            d["compositions"] = loadDictOfObj(d["compositions"],CompositionJSONDecoder)
            return dictToObject(d,"Project")
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")

def WorkspaceJSONDecoder(dct):
    if "_type" in dct and dct["_type"] == "Workspace":
        d = deserializeP1(dct)

        types = {"name":([str])}

        if checkDictType(d["projects"],uuid.UUID,Project) and checkDictForTypes(d,types):
            d["projects"] = loadDictOfObj(d["projects"],ProjectJSONDecoder)
            return dictToObject(d,"Workspace")
        raise MalformedFile("File did not pass the tests")
    raise MalformedFile("Bad type")
