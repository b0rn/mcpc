# -*- coding: utf-8 -*-

#####################
#				    #
#	14/03/2020		#
#	Victor Leveneur #
#					#
#####################

import json
from AbstractEntry import *
from Composition import *
from Entry import *
from Project import *
from Workspace import *

def dumpDictUUIDKey(dict,cls):
    ret = {}
    for uuid,o in dict.items():
        ret[str(uuid)] = json.dumps(o,cls=cls)
    return ret

class ProgressIndicatorJSONEncoder(json.JSONEncoder):
    def default(self,o):
        if isinstance(o,ProgressIndicator):
            ret = o.__dict__
            ret["uuid"] = str(ret["uuid"])
            ret["_type"] = "ProgressIndicator"
            return ret
        return json.JSONEncoder.default(self, o)

class AbstractEntryJSONEncoder(json.JSONEncoder):
    def default(self,o):
        if isinstance(o,AbstractEntry):
            ret = o.__dict__
            ret["uuid"] = str(ret["uuid"])
            ret["progress"] = json.dumps(ret["progress"],cls=ProgressIndicatorJSONEncoder)
            ret["_type"] = "AbstractEntry"
            return ret
        return json.JSONEncoder.default(self, o)

class ProjectJSONEncoder(AbstractEntryJSONEncoder):
    def default(self,o):
        if isinstance(o,Project):
            ret = super().default(o)

            ret["compositions"] = dumpDictUUIDKey(ret["compositions"],cls=CompositionJSONEncoder)

            ret["_type"] = "Project"
            return ret
        return json.JSONEncoder.default(self, o)

class CompositionJSONEncoder(AbstractEntryJSONEncoder):
    def default(self,o):
        if isinstance(o,Composition):
            ret = super().default(o)

            ret["entries"] = dumpDictUUIDKey(ret["entries"],cls=EntryJSONEncoder)

            ret["_type"] = "Composition"
            return ret
        return json.JSONEncoder.default(self, o)

class EntryJSONEncoder(AbstractEntryJSONEncoder):
    def default(self,o):
        if isinstance(o,Entry):
            ret = super().default(o)

            ret["progressIndicators"] = dumpDictUUIDKey(ret["progressIndicators"],cls=ProgressIndicatorJSONEncoder)

            ret["_type"] = "Entry"
            return ret
        return json.JSONEncoder.default(self, o)

class WorkspaceJSONEncoder(json.JSONEncoder):
    def default(self,o):
        if isinstance(o,Workspace):
            ret = o.__dict__

            ret["uuid"] = str(ret["uuid"])
            ret["projects"] = dumpDictUUIDKey(ret["projects"],cls=ProjectJSONEncoder)


            ret["_type"] = "Workspace"
            return ret
        return json.JSONEncoder.default(self, o)
