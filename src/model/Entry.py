# -*- coding: utf-8 -*-

#####################
#				    #
#	29/02/2020		#
#	Victor Leveneur #
#					#
#####################

from AbstractEntry import *

"""
Entry : can be a part of a track (like a riff)
"""

class Entry(AbstractEntry):
    def __init__(self,parentProgress,name,indicators,tags,start,length):
        super().__init__(name,start,length,tags,parentProgress)
        self.progressIndicators = {}
        for indicator in indicators:
            self.addIndicator(indicator)


    def addIndicator(self,name,comment):
        if isinstance(name,str):
            p = ProgressIndicator(self.progress,name,comment)
            self.progressIndicators[p.uuid] = p
            return p
    def rmIndicator(self,uuid):
        self.progressIndicators.pop(uuid,None)
