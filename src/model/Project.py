# -*- coding: utf-8 -*-

#####################
#				    #
#	03/03/2020		#
#	Victor Leveneur #
#					#
#####################

from AbstractEntry import *
from Composition import *

class Project(AbstractEntry):
    def __init__(self,name,tags,imagePath):
        super().__init__(name,0,None,tags,None)
        self.compositions = {}
        self.imagePath = imagePath if isinstance(imagePath,str) else None

    def addComposition(self,composition):
        if isinstance(entry,Composition):
            self.compositions[composition.uuid] = composition
            return composition
    def rmComposition(self,uuid):
        self.compositions.pop(uuid,None)
