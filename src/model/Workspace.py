# -*- coding: utf-8 -*-

#####################
#				    #
#	29/02/2020		#
#	Victor Leveneur #
#					#
#####################

from Project import *
import uuid

class Workspace():
    def __init__(self,name):
        self.uuid = uuid.uuid1()
        self.name = name if isinstance(name,str) else "New workspace"
        self.projects = {}

    def addProject(self,project):
        if isinstance(project,Project):
            self.projects[project.uuid] = project
            return project
    def rmProject(self,uuid):
        self.project.pop(uuid,None)
